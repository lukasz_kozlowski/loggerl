-module(loggerl_file_backend).

-behaviour(gen_event).

-export([init/1, handle_event/2, handle_call/2, handle_info/2, terminate/2, code_change/3]).

-record(loggerl_file_backend_state, {log_file_descriptor :: any(), current_log_level :: logger_utils:log_level()}).

-define(DEFAULT_LOG_LEVEL, info).



init(FileName) ->
    {ok, LogFileDescriptor} = file:open(FileName, [append, raw]),
    log_message_to_file(LogFileDescriptor, info, "Log started."),
    LoggerState = #loggerl_file_backend_state{log_file_descriptor = LogFileDescriptor, current_log_level = ?DEFAULT_LOG_LEVEL},
    {ok, LoggerState}.

handle_event({LogLevel, LogMessage}, #loggerl_file_backend_state{log_file_descriptor = LogFileDescriptor, current_log_level = CurrentLogLevel} = State) ->
    case loggerl_utils:is_loggable(LogLevel, CurrentLogLevel) of
        true ->
            log_message_to_file(LogFileDescriptor, LogLevel, LogMessage);
        false ->
            nothing
    end,
    {ok, State}.

handle_call({set_log_level, LogLevel}, State) ->
    case lists:member(LogLevel, loggerl_utils:log_levels()) of
        true ->
            {ok, ok, State#loggerl_file_backend_state{current_log_level = LogLevel}};
        false ->
            {ok, wrong_log_level, State}
    end;
handle_call(_Request, State) ->
    {ok, not_implemented, State}.

handle_info(_Info, State) ->
    {ok, State}.

terminate(_Args, #loggerl_file_backend_state{log_file_descriptor = LogFileDescriptor}) ->
    log_message_to_file(LogFileDescriptor, info, "Log stopped."),
    file:close(LogFileDescriptor).

code_change(_OldVersion, State, _Extra) ->
    {ok, State}.

log_message_to_file(LogFileDescriptor, LogLevel, Message) ->
    {{Year, Month, Day}, {Hour, Minute, Second}} = erlang:localtime(),
    LogMessage = io_lib:format("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B ~p: ~s~n",
                                [Year, Month, Day, Hour, Minute, Second, LogLevel, Message]),
    file:write(LogFileDescriptor, LogMessage).

-module(loggerl).

-behaviour(application).

-export([start/1, start/2, stop/0, stop/1]).
-export([debug/1, info/1, warning/1, error/1, fatal/1]).
-export([debug/2, info/2, warning/2, error/2, fatal/2]).
-export([set_log_level/1]).

-define(LOGGERL_EVENT_MANAGER_NAME, loggerl_event_manager).



start(LogFileName) ->
    application:start(loggerl),
    gen_event:add_handler(?LOGGERL_EVENT_MANAGER_NAME, loggerl_file_backend, LogFileName),
    ok.

start(normal, _StartArgs) ->
    gen_event:start({local, ?LOGGERL_EVENT_MANAGER_NAME}).

stop() ->
    gen_event:stop(?LOGGERL_EVENT_MANAGER_NAME).

stop(_State) ->
    stop().

debug(LogMessage) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {debug, LogMessage}).

debug(LogMessage, Args) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {debug, io_lib:format(LogMessage, Args)}).

info(LogMessage) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {info, LogMessage}).

info(LogMessage, Args) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {info, io_lib:format(LogMessage, Args)}).

warning(LogMessage) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {warning, LogMessage}).

warning(LogMessage, Args) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {warning, io_lib:format(LogMessage, Args)}).

error(LogMessage) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {error, LogMessage}).

error(LogMessage, Args) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {error, io_lib:format(LogMessage, Args)}).
    
fatal(LogMessage) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {fatal, LogMessage}).

fatal(LogMessage, Args) ->
    gen_event:notify(?LOGGERL_EVENT_MANAGER_NAME, {fatal, io_lib:format(LogMessage, Args)}).

set_log_level(LogLevel) ->
    gen_event:call(?LOGGERL_EVENT_MANAGER_NAME, logger_backend, {set_log_level, LogLevel}, infinity).
    

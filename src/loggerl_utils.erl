-module(loggerl_utils). 

-export([log_levels/0, log_level_to_number/1, number_to_log_level/1, is_loggable/2]).



-type log_level() :: debug | info | warning | error | fatal.
-type log_level_number() :: 0..4.

-export_type([log_level/0, log_level_number/0]).



log_levels() ->
    [debug, info, warning, error, fatal].

log_level_to_number(debug) -> 0;
log_level_to_number(info) -> 1;
log_level_to_number(warning) -> 2;
log_level_to_number(error) -> 3;
log_level_to_number(fatal) -> 4.

number_to_log_level(0) -> debug;
number_to_log_level(1) -> info;
number_to_log_level(2) -> warning;
number_to_log_level(3) -> error;
number_to_log_level(4) -> fatal.

is_loggable(LogLevel, CurrentLogLevel) ->
    log_level_to_number(LogLevel) >= log_level_to_number(CurrentLogLevel).
